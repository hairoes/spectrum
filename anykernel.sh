# AnyKernel3 Ramdisk Mod Script
# osm0sis @ xda-developers

## AnyKernel setup
# begin properties
properties() { '
kernel.string=Spectrum Support for DesertEagle Kernel
do.devicecheck=1
do.modules=0
do.cleanup=1
do.cleanuponabort=0
device.name1=whyred
device.name2=wayne
device.name3=tulip
device.name4=
device.name5=
supported.versions=8,9,10
supported.patchlevels=
'; } # end properties

# shell variables
block=/dev/block/bootdevice/by-name/boot;
is_slot_device=auto;
ramdisk_compression=auto;

## AnyKernel methods (DO NOT CHANGE)
# import patching functions/variables - see for reference
. tools/ak3-core.sh;


## AnyKernel file attributes

## AnyKernel install 
dump_boot;

# begin ramdisk changes
# init.rc

android_ver=$(file_getprop /system/build.prop "ro.build.version.release") 
max_freq=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq)
if [ $max_freq == 1612800 ];  then 
	sbin="NONOC"
else 
	sbin="OC"
fi;
if [ $android_ver == "10" ]; then
	# migrate from /overlay to /overlay.d to enable SAR Magisk
	if [ -d $ramdisk/overlay ]; then
		rm -rf $ramdisk/overlay;
	fi;
	cp -f /tmp/anykernel/init/init.spectrum.rc /tmp/anykernel/ramdisk/overlay.d/init.spectrum.rc
	cp -f /tmp/anykernel/init/$sbin /tmp/anykernel/ramdisk/overlay.d/sbin/spectrum4de
else
	backup_file init.rc;
	grep "import /init.spectrum.rc" init.rc >/dev/null || sed -i '1,/.*import.*/s/.*import.*/import \/init.spectrum.rc\n&/' init.rc;
	cp -f /tmp/anykernel/init/init.spectrum.rc /tmp/anykernel/ramdisk/init.spectrum.rc
	cp -f /tmp/anykernel/init/$sbin /tmp/anykernel/ramdisk/sbin/spectrum4de
fi;

# set permissions/ownership for included ramdisk files
set_perm_recursive 0 0 755 750 $ramdisk/*;


# end ramdisk changes
write_boot;
## end install

